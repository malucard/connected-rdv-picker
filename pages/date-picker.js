import {useState} from 'react'
import { gapi } from 'gapi-script';

//new date import
import { Badge } from "@material-ui/core";
import { DatePicker , MuiPickersUtilsProvider} from '@material-ui/pickers';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
 
export default function DatePickerFct(props){
    const [selectedDays, setSelectedDays] = useState([1, 2, 15]);
    const [selectedDate, setSelectedDate] = useState(new Date());
    const [selectedTime, setSelectedTime] = useState(new Date());
  
    const handleDateChange = (date)=>{
      setSelectedDate(date);
      props.onDatePicked(selectedDate);
    }
    const handleTimeChange = (time)=>{
      console.log("Selected Time",time);
      setSelectedTime(time);
    }
    const onRenderDays = (day, selectedDate, isInCurrentMonth, dayComponent)=>{
        /*const isSelected = isInCurrentMonth;
          // You can also use our internal <Day /> component
        return <Badge badgeContent={isSelected ? "🌚" : undefined}>{dayComponent}</Badge>;*/
        //console.log("selected Date:",day);
        return dayComponent
    }
    const handleMonthChange = async (mouth)=>{
        return new Promise(resolve => {
          //getFreeBusy(selectedDays, selectedDays);
            /*setTimeout(() => {
              setSelectedDays([1, 2, 3].map(() => getRandomNumber(1, 28)));
              resolve();
            }, 1000);*/
          });
    }

    return(
    <div className="App custum-d-picker-style">
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <DatePicker
            disableToolbar
            variant="static"
            label="Only calendar"
            helperText="No year selection"
            value={selectedDate}
            renderDay={onRenderDays}
            shouldDisableDate={filterWeekends}
            onChange={handleDateChange}
            onMonthChange={handleMonthChange}
        />
      </MuiPickersUtilsProvider>
    </div>
    );
}