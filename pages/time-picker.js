import {useState , useEffect} from 'react'
import { TimePicker , MuiPickersUtilsProvider} from '@material-ui/pickers';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';

export default function TimePickerFct(props){
    const [selectedDate, setSelectedDate] = useState(props.curenSelection);
    const handleDateChange = (date)=>{
        console.log("Selected Time",date);
        setSelectedDate(date);
        props.setTime(selectedDate);
      }
      useEffect(() => {
        setSelectedDate(props.curenSelection);
      },[props.curenSelection]);
    return(
        <div className="App custum-d-picker-style">
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <TimePicker
                showTodayButton
                todayLabel="now"
                variant="static"
                value={selectedDate}
                minutesStep={5}
                onChange={handleDateChange}
            />
          </MuiPickersUtilsProvider>
        </div>
        );
}