import {useState, useEffect} from 'react'
import MenuItem from '@material-ui/core/MenuItem';
import DatePickerFct from './date-picker'
import TimePickerFct from './time-picker'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

var clasnameList = {
    maicontainer: "maincontainer d-flex flex-column justify-content-center align-items-center",
    seleccontainer: "select-container d-flex flex-row justify-content-center",
    selectPers:"container select-pers d-flex flex-column",
    logo_container:"logo-container d-flex align-items-center flex-column",
    logo_img:"logo-img",
    photoContainer:"photo-container",
    photoImg:"photo-img d-flex flex-column justify-content-center align-items-center",
    divImg:"d-flex flex-column justify-content-center align-items-center img-blank",
    pfImg:"pf-img",
    bodyPrs:"container body-prs d-flex flex-column",
    namecontainer:"name-container",
    titleContainer:"title-container",
    time:"time-container d-flex flex-row align-items-center",
    decription:"desc-cont",
    selectDate:"select-date d-flex flex-column",
    titlecontainer:"title-container-date d-flex flex-column align-items-center",
    customSelect:"custom-select",
}
const GOOGLE_API_KEY = 'AIzaSyDBPYy3rYr8S55VMN3k2g7bJDU_aPh9PFY';
const CALENDAR_ID = 'r.dagoswat@gmail.com';

function showTimeRenderer(condition,efunciton,selDate){
    return condition? <div className="time-picker-container animate__animated animate__fadeIn">
                        <TimePickerFct setTime={efunciton} curenSelection={selDate}></TimePickerFct>
                      </div> : null;
}
function showValidationButton(condition,selDate){
    return condition? <Button className="animate__animated animate__bounceIn" variant="outlined" color="primary">
                            Confirmer
                      </Button> : null
}
function showZoneList(condition,liste,listSelected,listSelectionChange){
    let clist = liste.map((itm,i)=>{
        return <MenuItem value={itm.countryCode + itm.timestamp} key={itm.countryCode + "-id:" + i}>{itm.zoneName}</MenuItem>;
    });
    return condition?
    <Autocomplete
      id="combo-box-demo"
      options={liste}
      getOptionLabel={(option) => option.zoneName}
      renderInput={(params) => <TextField {...params} label="Fuseau horaire actuel:" variant="outlined"
      value={listSelected}
      onChange={(event, newValue) => {
        console.log("New value",newValue);
      }}/>}
    /> : 
    <div class="gooey">
        <span class="dot"></span>
        <div class="dots">
        <span></span>
        <span></span>
        <span></span>
        </div>
    </div>
}
//GOOGLE APIS
function getEvent(){
    function start() {
        gapi.client.init({
          'apiKey': GOOGLE_API_KEY
        }).then(function() {
          return gapi.client.request({
            'path': `https://www.googleapis.com/calendar/v3/calendars/${CALENDAR_ID}/events`,
          })
        }).then( (response) => {
            let events = response.result.items
            console.log("API=====>",events);
        }, function(reason) {
          console.log(reason);
        });
      }
      console.log("button pressed");
      gapi.load('client', start);
}
function getFreeBusy(dateMin,dateMax){
    function start() {
        gapi.client.init({
          'apiKey': GOOGLE_API_KEY
        }).then(function() {
          return gapi.client.request({
            'path': `https://www.googleapis.com/calendar/v3/freeBusy`,
            'params':{
                "timeMax": dateMax,
                "timeMin": dateMin
            }
          })
        }).then( (response) => {
            let events = response.result.items
            console.log("API=====>",events);
        }, function(reason) {
          console.log(reason);
        });
      }
      gapi.load('client', start);
  }
//=================================
export default function MainPage(){
    const [selectedDate, setSelectedDate] = useState(new Date());
    const [selectedTime, setSelectedTime] = useState(new Date());
    const [showTime, setShowTime] = useState(false);
    const [showValidation, setShowValidation] = useState(false);
    const [listSelected, setSelectedList] = useState("");
    const [zoneList, setZoneList] = useState([]);
    const [showZones, setShowZones] = useState(false);
    
    useEffect(() => {
        setShowTime(false);
        console.log("show state",showTime);
        setShowZones(false);
        fetch("http://api.timezonedb.com/v2.1/list-time-zone?key=J4L7Q27D1F2N&format=json")
          .then(res => res.json())
          .then(
            (result) => {
                console.log("Time loaded,",result);
                setZoneList(result.zones);
                setShowZones(true);
            },
            (error) => {
              alert("Erreur chargement Zone.");
              setShowZones(true);
            }
          )
    },[]);
   
    const setDateSelection = (date)=>{
        console.log("Emited curent Date",date);
        setSelectedDate(date);
        setShowTime(true);
    }

    const setTimeSelection = (time)=>{
        setSelectedTime(time);
        setShowValidation(true);
    }
    const listSelectionChange = (event, selection)=>{
        console.log("selected item",selection);
        //setSelectedList(selection.target.value);
    }
    function showPicker(condition){
        return condition?
        <div className="time-select-container">
            {showTimeRenderer(showTime,setTimeSelection,selectedDate)}
            {showValidationButton(showValidation,selectedTime)}
        </div> : null;
    }
    return(
        <div data-pg-name="maicontainer" className={clasnameList.maicontainer}>
            <div data-pg-name="seleccontainer" className={clasnameList.seleccontainer}>
                <div className={clasnameList.selectPers} data-pg-name="select-pers" style={{maxWidth: "350px"}}>
                    <div className={clasnameList.logo_container} data-pg-name="logo_container">
                        <img className={clasnameList.logo_img} src="images.png"/>
                    </div>
                    <div className={clasnameList.photoContainer} data-pg-name="photo-container">
                        <div className={clasnameList.photoImg} data-pg-name="photo-img">
                            <div className={clasnameList.divImg}>
                                <img src="chris-hemsworth-ig-480x360.jpg" data-pg-name="pf-img" className={clasnameList.pfImg}/>
                            </div>
                        </div>
                    </div>
                    <div className={clasnameList.bodyPrs} data-pg-name="body-prs">
                        <div className={clasnameList.namecontainer} data-pg-name="namecontainer">
                            <p>Mahery RAZAF</p>
                        </div>
                        <div data-pg-name="titlecontainer" className={clasnameList.titleContainer}>
                            <p style={{marginBottom: "0"}}>Titre Teste</p>
                        </div>
                        <div className={clasnameList.time} data-pg-name="time">
                            <img src="Asset%201.svg" className="tm-img"/>
                            <p style={{marginLeft: "9px", marginBottom: "0"}}>30 mn</p>
                        </div>
                        <div className={clasnameList.decription} data-pg-name="decription">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                        </div>
                        <Button onClick={()=>{getFreeBusy("2020-10-12T07:20:50.52Z","2020-10-12T07:20:50.52Z")}} variant="outlined" color="primary">
                            Connexion
                        </Button>
                        <div className="form-controle-select"></div>
                    </div>
                </div>
                <div className={clasnameList.selectDate} data-pg-name="select-date">
                    <div className={clasnameList.titlecontainer} data-pg-name="titlecontainer">
                        <p style={{marginBottom: "0"}}>Sélectionnez la date et l'heure</p>
                    </div>
                    <DatePickerFct onDatePicked={setDateSelection}></DatePickerFct>
                    <div className="form-controle-select"></div>
                    {showZoneList(showZones,zoneList,listSelected,listSelectionChange)}
                </div>
                {showPicker(showTime)}
            </div>
        </div>
    );
}