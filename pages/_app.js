//import global styles
import '../styles/globals.css'
import '../styles/bootstrap.min.css'
import '../styles/custom.css'
import '../styles/animate.min.css'
import '../styles/custum-animation.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
