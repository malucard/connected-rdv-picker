import MainPage from './main-page'
import Header from './head'

export default function Home() {

  return (
    <div>
      <Header></Header>
      <MainPage></MainPage>
    </div>
  )
}
